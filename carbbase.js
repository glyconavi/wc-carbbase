import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/iron-location/iron-location.js';

class GlyconaviCarbBase extends PolymerElement {
  static get template() {
    return html`
<style type="text/css">

.glyconavi_table_01{

}
.glyconavi_table_01 th{
width: 25%;
padding: 6px;
text-align: left;
vertical-align: top;
color: #333;
background-color: #eee;
border: 0.5px solid whitesmoke;
}
.glyconavi_table_01 td{
padding: 6px;
background-color: #fff;
border: 0.5px solid whitesmoke;
}

.glyconavi_table_tfoot tfoot{
border: 0px;	
}
.glyconavi_table_tfoot tr{
border: 0px;	
}
.glyconavi_table_tfoot td{
border: 0px;
text-align: right;	
}
.glyconavi_table_radio td{
	text-align: left;	
	width: 200;
}
.glyconavi_table_radio tr{
	text-align: left;	
	width: 200;
}


</style>
 
<iron-location path="{{path}}" hash="{{hash}}" query="{{query}}" dwell-time="{{dwellTime}}"></iron-location>
<iron-ajax auto="" url="https://test.sparqlist.glyconavi.org/api/PDB-pdbid?[[query]]" handle-as="json" last-response="{{iddata}}"></iron-ajax>
<iron-ajax auto="" url="https://test.sparqlist.glyconavi.org/api/PDB-wc-carbbase-chemcomp?[[query]]" handle-as="json" last-response="{{resultdata}}"></iron-ajax>

<div>
  <table class="glyconavi_table_01">
  <caption>
    <b><a>Chemical Component: </a><a href="http://pdbj.org/chemie/summary/[[retrieveId(query)]]" target="_blank">[[retrieveId(query)]]</a>
    </b>
    <details>
      <summary>Notes</summary>
      <p>from NBDC RDF Bioportal</p>
    </details>
  </caption>
  <tr>
    <!-- 表示非表示切り替え -->
    <tr>
      <td>chemical name</td>
      <td>
        <!-- PDBj -->
      <template is="dom-repeat" items="[[resultdata]]">
        [[item.chem_comp_name]]
      </template>
      </td>
    </tr>
    <tr><td>formula weight</td><td>
      <template is="dom-repeat" items="[[resultdata]]">
        [[item.formula_weight]]
      </template>
    </td></tr>
    <tr><td>type</td><td>
      <template is="dom-repeat" items="[[resultdata]]">
        [[item.chem_comp_type]]
      </template>
    </td></tr>
  </tr>
  <tr>
    <td>PDB</td>
    <td>
      <template is="dom-repeat" items="[[iddata]]">
        <a href="http://www.pdbj.org/pdb/[[item.pdbid]]" target="_blank" >[[item.pdbid]]</a>
      </template>
    </td>
  </tr>
  <tfoot class="glyconavi_table_tfoot">
    <tr>
      <td colspan="2"><small>from <a href="http://wwpdb.org" target="_blank">wwPDB</a></small></td>
    </tr>
  </tfoot>
  </table>
</div >
`;
  }
  constructor() {
    super();
  }
  static get properties() {
    return {
      resultdata: {
        notify: true,
        type: Object,
        value: function() {
          return new Object();
        }
      }
    };
  }

  _handleAjaxPostResponse(e) {
    console.log(e);
  }
  _handleAjaxPostError(e) {
    console.log('error: ' + e);
  }
  retrieveId(query) {
    console.log(query);
    var params = query.split("=");

    return params[1];
  }
}

customElements.define('wc-carbbase', GlyconaviCarbBase);
