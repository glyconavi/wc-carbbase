Overall component for each [entry](http://carb3d.glyconavi.org/pdbcc_entry.php?id=NAG) in carb3d.

Visit demo/index.html to see live examples of your element running.


* https://glyconavi.gitlab.io/wc-carbbase/?id=BMA